
require 'sinatra/base'
require 'sinatra/custom_logger'
require 'logger'

require 'switcher/version'
require 'switcher/app/utils/machine'

require 'yaml'
require 'net/ping'

module Switcher
  class Application < Sinatra::Base
    helpers Sinatra::CustomLogger

    configure do
      logger = Logger.new(File.open("switcher-#{environment}.log", 'a'))
      logger.level = Logger::DEBUG if development?
      set :logger, logger
    end

    set :static, true                             # set up static file routing
    set :public_dir, File.expand_path('..', __FILE__) # set up the static dir (with images/js/css inside)

    set :views,  File.expand_path('../views', __FILE__) # set up the views dir

    set :bind, '0.0.0.0'

    get '/' do
     "switcher: v#{Switcher::VERSION}"
    end

    post '/wake/:name' do
      machine = Switcher::Machine.new(name: params[:name])
      machine.wake
      redirect("waking/#{params[:name]}")
    end

    post '/sleep/:name' do
      machine = Switcher::Machine.new(name: params[:name])
      machine.shutdown
      redirect("sleeping/#{params[:name]}")
    end

    get '/machines'do
      Switcher::Machine.machines
    end

    get '/ping/:name'do
      machine = Switcher::Machine.new(name: params[:name])
      machine.ping
    end

    get '/waking/:name' do
      "Waking #{params[:name]}"
    end

    get '/sleeping/:name' do
      "Turning off #{params[:name]}"
    end
  end
end


