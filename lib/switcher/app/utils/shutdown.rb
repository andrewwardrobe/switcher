require 'net/ssh'

module Switcher::Shutdown
  def windows_shutdown(ip, user, ssh_key)
    Net::SSH.start(ip, user, keys: [ssh_key]) do |ssh|
      ssh.exec!('shutdown -p')
    end
  end

  def linux_shutdown(ip, user, ssh_key)
    Net::SSH.start(ip, user, keys: [ssh_key]) do |ssh|
      ssh.exec!('sudo shutdown -P now')
    end
  end
end