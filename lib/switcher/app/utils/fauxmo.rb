require 'json'

module Switcher
  class Fauxmo

    def initialize(machines)

      devices = []
      machines.each do |machine, data|
        puts "machine #{data.to_yaml}"
        name = data[:name] ? data[:name] : machine
        devices << {
            name: name,
            on_cmd: "http://localhost:4567/wake/#{name}",
            off_cmd: "http://localhost:4567/sleep/#{name}",
            state_cmd: "http://localhost:4567/ping/#{name}",
            method: 'POST',
            state_method: 'GET',
            state_response_on: 'on',
            state_response_off: 'off',
            port: data[:port]
        }
      end
      @config = {
          FAUXMO: {
              ip_address: 'auto'
          },
          PLUGINS: {
              SimpleHTTPPlugin: {
                  DEVICES: devices
              }
          }
      }
    end

    def config
      JSON.pretty_generate(@config)
    end

    def print_config
      puts config
    end



    def launch(location)
      puts "Launching Fauxmo..."
      Thread.new do
        system(launch_scripts(location))
      end
    end

    def save_config(path)
      File.open(path,'w') do |file|
        file.write config
      end
    end

  private
    def launch_scripts(location)
      <<~HEREDOC
        cd #{location}
        . venv/bin/activate
        fauxmo -c #{location}/config.json
      HEREDOC
    end
  end
end