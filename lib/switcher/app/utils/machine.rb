require 'wol'
require 'yaml'
require 'net/netconf'
require 'switcher/app/utils/shutdown'

module Switcher
  class Machine
    include Switcher::Shutdown
    attr_accessor :ip, :mac, :type, :user, :sshkey
    def self.load_machines(machines)
      @@machines = machines
    end

    def self.machines
      @@machines.collect {|key, val| key.to_s}
    end

    def initialize(options = {})
      if options[:name]
        @@machines[options[:name].intern].each do |key, value|
          instance_variable_set("@#{key}".to_sym, value)
        end
      end
    end

    def wake
      Wol::WakeOnLan.new(mac: @mac).wake
    end

    def ping
      if Net::Ping::External.new.ping?(@ip)
        'on'
      else
        'off'
      end
    end

    def shutdown
      case @type
        when 'windows'
          windows_shutdown(@ip, @user, @sshkey)
        when 'linux'
          linux_shutdown(@ip, @user, @sshkey)
      end
    end
    def debug_vars
      puts self.to_yaml
    end
  end
end