require "switcher/version"
require "switcher/app/controllers/app"
require 'switcher/app/utils/machine'
require 'switcher/app/utils/fauxmo'

module Switcher

  def self.launch_fauxmo(config)
    fauxmo = Switcher::Fauxmo.new(config[:machines])
    fauxmo.save_config("#{config[:fauxmo_location]}/config.json")
    fauxmo.launch ("#{config[:fauxmo_location]}")
  end
  # Your code goes here...
  def self.main(config = {})
    Switcher::Machine.load_machines(config[:machines])
    launch_fauxmo(config) if config[:launch_fauxmo]
    Switcher::Application.run!
  end
end

