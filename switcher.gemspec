
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "switcher/version"

Gem::Specification.new do |spec|
  spec.name          = "switcher"
  spec.version       = Switcher::VERSION
  spec.authors       = ["Andrew Wardrobe"]
  spec.email         = ["andrew.g.wardrobe@googlemail.com"]

  spec.summary       = "Provides a REST API for WOLing your computers"
  spec.description   = "Provides a REST API for WOLing your computers"
  spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rubocop", "~> 0.52.1"

  spec.add_runtime_dependency "sinatra", "2.0.1"
  spec.add_runtime_dependency "sinatra-contrib", "2.0.1"
  spec.add_runtime_dependency "wol", "0.4.1"
  spec.add_runtime_dependency "net-ping", "2.0.2"
  spec.add_runtime_dependency "netconf", "0.3.1"
  spec.add_runtime_dependency "net-ssh", "~> 4.2"
  spec.add_runtime_dependency "json", "~> 2.1"
end
